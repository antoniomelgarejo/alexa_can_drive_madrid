/* eslint-disable  func-names */
/* eslint-disable  no-console */
"use strict";
const Alexa = require('ask-sdk-core');
const request = require('request');
const https = require('https');
const mockupResponse = {
  "category": "c",
  "scenario": {
    "today": 0,
    "tomorrow": 0
  },
  "restrictions": {
    "today": {
      "park": true,
      "driveCenter": true,
      "driveCity": true
    },
    "tomorrow": {
      "park": false,
      "driveCenter": true,
      "driveCity": false
    }
  }
};

function httpGet() {
  return new Promise(((resolve, reject) => {
    const options = {
      host: 'api.icndb.com',
      port: 443,
      path: '/jokes/random',
      method: 'GET',
    };

    const request = https.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        resolve(JSON.parse(returnData));
      });

      response.on('error', (error) => {
        reject(error);
      });
    });
    request.end();
  }));
}

function httpGetDriveInfo(license) {
  return new Promise(((resolve, reject) => {
    const options = {
      host: 'puedocircular.com',
      port: 443,
      path: `/api/v2/license/${license}`,
      method: 'GET',
    };

    const request = https.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        resolve(JSON.parse(returnData));
      });

      response.on('error', (error) => {
        reject(error);
      });
    });
    request.end();
  }));
}


const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const speechText = 'Tell me your license please';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const HelloWorldIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'HelloWorldIntent';
  },
  async handle(handlerInput) {
    const response = await httpGet();

    console.log(response);

    return handlerInput.responseBuilder
      .speak("Okay. Here is what I got back from my request. " + response.value.joke)
      .reprompt("What would you like?")
      .getResponse();
  },
};

const CanDriveIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'CanDriveIntent';
  },
  async handle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    const license = request.intent.slots.license.value;
    const response = await httpGetDriveInfo(license);

    console.log(response);
    const r = mockupResponse.restrictions;
    const speech = `Today, you ${r.today.driveCenter ? 'can': 'cannot'} drive in the centre,
    you ${r.today.driveCity ? 'can': 'cannot'} drive in the city,
    you ${r.today.park ? 'can': 'cannot'} park in S.E.R. zone.
    Tomorrow, you ${r.tomorrow.driveCenter ? 'can': 'cannot'} drive in the centre,
    you ${r.tomorrow.driveCity ? 'can': 'cannot'} drive in the city,
    you ${r.tomorrow.park ? 'can': 'cannot'} park in S.E.R. zone.
    `;
    return handlerInput.responseBuilder
      .speak(speech)
      .reprompt("What would you like?")
      .getResponse();
  },
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'You can say hello to me!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Goodbye!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    HelloWorldIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler,
    CanDriveIntentHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
